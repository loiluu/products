from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.conf.urls import patterns, include, url
import views, settings
from django.views.static import serve
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^register/$', views.register, name="register"),
    url(r'^login/$', views.user_login, name="login"),
    url(r'^create_product/$', views.create_product, name="create_product"),
    url(r'^create_category/$', views.create_category, name="create_category"),
    url(r'^create_material/$', views.create_material, name="create_material"),
    url(r'^create_manufacturer/$', views.create_manufacturer, name="create_manufacturer"),
    url(r'^view_product/(?P<product_id>\w+)/qr$', views.view_qrcode, name="view_product_qr"),
    url(r'^view_product/(?P<product_id>\w+)', views.view_product, name="view_product"),
    url(r'^activate/(?P<key>[a-zA-Z0-9_.-]+)/$', views.activate, name='activate'),
    url(r'^view_categories/$', views.view_categories, name="view_categories"),
    url(r'^view_materials/$', views.view_materials, name="view_materials"),
    url(r'^view_manufacturers/$', views.view_manufacturers, name="view_manufacturers"),
    url(r'^product_in_material/(?P<material_id>\w+)$', views.view_product_in_material, name="product_in_material"),
    url(r'^product_in_category/(?P<category_id>\w+)$', views.view_product_in_category, name="product_in_category"),
    url(r'^product_in_manufacturer/(?P<manufacturer_id>\w+)$', views.view_product_in_manufacturer, \
        name="product_in_manufacturer"),
    url(r'^all_products/$', views.all_products, name="all_products"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'},name="logout"),
    url(r'^change_password/$', views.change_password, name="change_password"),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^products/static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),
    url(r'^products/media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),

        # Examples:
    # url(r'^$', 'products.views.home', name='home'),
    # url(r'^products/', include('products.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
