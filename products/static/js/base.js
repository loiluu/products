/**
 * Created with PyCharm.
 * User: Admin
 * Date: 11/15/12
 * Time: 4:22 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {
    $.fn.enabelNotification = function () {
        $(".notify-widget-pane").show();
    };

    $.fn.showNotification = function (msg, duration) {
        $("#notify").enabelNotification();
        $("#notify").text(msg);
        $("#notify").fadeIn('fast');
        if (!duration || typeof duration != 'number') duration = 3000;
        $("#notify").data('delay', setTimeout(function () {
                $("#notify").stop(true, true).fadeOut('fast');
            },
            duration));
    };

    $.ajaxSetup({
        beforeSend:function (xhr, settings) {
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != "") {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }

            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        }
    });
    $('.dropdown-toggle').dropdown();
});