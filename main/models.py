from django.db import models
# -*- coding: utf-8 -*-
from django.db import models
from django.forms.models import ModelChoiceField
import datetime
from django.contrib.auth.models import User
from django.contrib import admin
# Create your models here.

USER_STATUS = ((1, 'Active'), (2, 'Inactive'))
REGISTER_STATUS = (('Active', 'Active'), ('Inactive', 'Inactive'))
USER_PRIVILEDGES = ((1, 'Normal Member'), (2, 'Mod'))
PRODUCT_STATUS = ((1, 'Activated'), (2, 'Inactivated'))
# Create your models here.


class Register(models.Model):
    fullname = models.CharField('Fullname', max_length=250, blank=False)
    email= models.EmailField("Email", max_length=250, unique=True)
    status = models.CharField("Status", max_length=10, default='Inactive',
        choices=REGISTER_STATUS)
    register_date = models.DateField("Registration Date", default=datetime.date.today)
    username = models.CharField("Username", max_length=30, unique=True)
    activation_code = models.CharField("Registration code", max_length=30, default="")

    def __unicode__(self):
        return unicode(self.fullname)

    class Meta:
        verbose_name = u"Register"
        verbose_name_plural = u"Registers"


class Account(models.Model):
    username = models.OneToOneField(User, verbose_name="Username")
    fullname = models.CharField("Fullname", max_length=250, blank=False)
    email = models.EmailField("Email", max_length=250, unique=True)
    type = models.SmallIntegerField("User type", max_length=3, default=1,\
        choices=USER_PRIVILEDGES)
    status = models.IntegerField("Status", max_length=3, default=1,\
        choices=USER_STATUS)

    def __unicode__(self):
        return unicode(self.fullname)

    def get_info(self):
        return unicode(self.fullname) + u" - " + unicode(self.username)

    class Meta:
        verbose_name = u"Account"
        verbose_name_plural = u"Account"


class Manufacturer(models.Model):
    name = models.CharField("Name", max_length=50, unique=True, blank=False)
    description = models.CharField("Description", max_length=100)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = "Manufacturer"
        verbose_name_plural = "Manufacturer"


class Material(models.Model):
    name = models.CharField("Name", max_length=50, unique=True, blank=False)
    description = models.CharField("Description", max_length=100)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = "Material"
        verbose_name_plural = "Material"

class Category(models.Model):
    name = models.CharField("Category name", max_length=50, unique=True, blank=False)
    description = models.CharField("Description", max_length=100)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = "Product category"
        verbose_name_plural = "Product category"


class Product(models.Model):
    user_id = models.ForeignKey(User, verbose_name="Uploader", null=True)
    name = models.CharField("Name", default='Unnamed product', max_length=100)
    category_id = models.ForeignKey(Category, verbose_name=u"Category", null=True, blank=True)
    manufacturer_id = models.ForeignKey(Manufacturer, verbose_name=u"Manufacturer", null=True, blank=True)
    material_id = models.ForeignKey(Material, verbose_name=u"Material", null=True, blank=True)
    created_date = models.DateField("Created date", default=datetime.date.today)
    status = models.SmallIntegerField("Display status",
        max_length=3, choices=PRODUCT_STATUS, default=1)
    description = models.CharField("Description", max_length=200, blank=True)
    value = models.FloatField ("Estimated value", default=0)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Product"


admin.site.register(Account)
admin.site.register(Register)
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Manufacturer)
admin.site.register(Material)