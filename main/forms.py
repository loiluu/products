from models import *
from django import forms

class RegisterForm(forms.ModelForm):
    class Meta:
        model = Register
        exclude = {'register_date', 'status', 'activation_code'}


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = {'user_id', 'status', 'qrcode'}


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category


class ManufacturerForm(forms.ModelForm):
    class Meta:
        model = Manufacturer


class MaterialForm(forms.ModelForm):
    class Meta:
        model = Material

class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(label=u'Old password',
            widget=forms.PasswordInput)
    new_password = forms.CharField(label=u'New password',
            widget=forms.PasswordInput)
    re_password = forms.CharField(label=u'Re-enter new password',
            widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean_password(self):
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(u"Wrong password")
        return old_password

    def clean_re_password(self):
        new_password = self.cleaned_data['new_password']
        re_password = self.cleaned_data['re_password']
        if new_password != re_password:
            raise forms.ValidationError(u'Re-password does not match')
        return re_password

    def save(self):
        cleaned_data = super(ChangePasswordForm, self).clean()
        new_password = cleaned_data['new_password']
        self.user.set_password(new_password)
        self.user.save()